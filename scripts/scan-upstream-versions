#!/bin/sh

DIR=/var/www/repo/pool
OUTFILE=/var/www/packages/.upstream-versions

scan_upstream_version() {
    DEBFILE=$1
    UPVERSION=

    DEBNAME=$(basename "$DEBFILE" | sed 's/_.*$//')

    # Kernels are special: we want the latest release for the current branch,
    # not the latest released kernel.
    if echo "$DEBNAME" | grep -q "^linux-"; then
        KVER=$(echo "$DEBNAME" | sed -r 's/.*-([0-9]+(\.[0-9]+)+).*/\1/')
        KPATCH=$(wget -q -O - https://www.kernel.org/pub/linux/kernel/v5.x/ | grep "patch-$KVER" | sed -r "s/.*$KVER\.([0-9]+).*/\1/" | sort -n | tail -1)
        if echo "$KPATCH" | grep -q "patch-$KVER"; then
            KPATCH=0
        fi
        [ "$KPATCH" ] && UPVERSION="$KVER.$KPATCH"
    else
        # For normal packages, we extract the debian folder and simply run uscan
        TMPDIR=$(mktemp -d /tmp/uscan.XXXXXXXX)
        WORKDIR="$PWD"
        cd "$TMPDIR" || return
        tar xJf "$DEBFILE"
        UPVERSION=$(uscan --report --dehs 2>/dev/null | grep "upstream-version" | sed -r 's%^.*>(.*)</.*$%\1%')
        cd "$WORKDIR" || return
        rm -rf "$TMPDIR"
    fi

    if [ "$UPVERSION" ]; then
        if grep -q "$DEBNAME" "$OUTFILE"; then
            FILEVERSION=$(grep "$DEBNAME," $OUTFILE | cut -d ',' -f 2)
            # We might have multiple versions of a package, so let's store only the
            # latest one
            if dpkg --compare-versions "$UPVERSION" gt "$FILEVERSION"; then
                sed -i "s/$DEBNAME,.*/$DEBNAME,$UPVERSION/" "$OUTFILE"
            fi
        else
            echo "$DEBNAME,$UPVERSION" >> "$OUTFILE"
        fi
    fi
}

touch "$OUTFILE"

for file in "$DIR"/*/*/*/*.debian.tar.xz; do
    scan_upstream_version "$file"
done
