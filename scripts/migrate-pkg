#!/bin/sh

usage()
{
    echo "Usage: $0 [-n] PACKAGES" 1>&2
    echo "    -n        Work on non-free repositories" 1>&2
    echo "    PACKAGES  List of packages to migrate, separated with whitespaces" 1>&2
    exit "$1"
}

COMPONENT="main"

while getopts "hn" opt
do
    case "$opt" in
        n)
            COMPONENT="non-free"
            ;;
        h)
            usage 0
            ;;
        *)
            usage 1
            ;;
    esac
done

# Remove all options from arguments so we're left only with package names
shift $((OPTIND-1))

if [ $# -eq 0 ]; then
    usage 1
fi

# shellcheck disable=SC2068
for PACKAGE in $@
do
    echo "Migrating package $PACKAGE to bookworm-$COMPONENT..."

    echo "Removing previous version..."
    aptly repo remove mobian-bookworm-$COMPONENT "\$Source ($PACKAGE)" &&
    aptly repo remove mobian-bookworm-$COMPONENT "Name ($PACKAGE)" || exit 1

    echo "Copying new version..."
    aptly repo copy mobian-unstable-$COMPONENT mobian-bookworm-$COMPONENT "\$Source ($PACKAGE)" &&
    aptly repo copy mobian-unstable-$COMPONENT mobian-bookworm-$COMPONENT "Name ($PACKAGE)" || exit 1
done

echo "Updating archive..."
aptly publish update bookworm filesystem:mobian: || exit 1
