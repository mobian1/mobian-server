package main

import (
    "encoding/csv"
    "fmt"
    "log"
    "os"
    "sort"
    "strings"

    "github.com/aptly-dev/aptly/database"
    "github.com/aptly-dev/aptly/database/goleveldb"
    "github.com/aptly-dev/aptly/deb"
    aptlyhttp "github.com/aptly-dev/aptly/http"
    "github.com/aptly-dev/aptly/pgp"
    "github.com/aptly-dev/aptly/query"
)

type mobianPkg struct {
    name string
    dTesting string
    dUnstable string
    mStable string
    mUnstable string
    upstream string
}

var upstreamCsv [][]string

func buildRepo(name, url, dist string, component string, db database.Storage) (*deb.PackageList, error) {
    var comp []string
    comp = append(comp, component)

    mobianrr, err := deb.NewRemoteRepo(name, url, dist, comp, []string{}, true, false, false)
    if err != nil {
        return nil, err
    }
    dl := aptlyhttp.NewDownloader(0, 1, nil)
    finder := pgp.GPG2Finder()
    verifier := pgp.NewGpgVerifier(finder)
    verifier.AddKeyring("/usr/share/keyrings/debian-archive-keyring.gpg")
    err = verifier.InitKeyring()
    if err != nil {
        return nil, err
    }
    mobianCollectionFactory := deb.NewCollectionFactory(db)
    err = mobianrr.DownloadPackageIndexes(nil, dl, verifier, mobianCollectionFactory, true)
    if err != nil {
        return nil, err
    }
    err = mobianrr.FinalizeDownload(mobianCollectionFactory, nil)
    if err != nil {
        return nil, err
    }

    mobianRefList := mobianrr.RefList()
    mobianPkgCollection := deb.NewPackageCollection(db)
    pkgList, err := deb.NewPackageListFromRefList(mobianRefList, mobianPkgCollection, nil)
    if err != nil {
        return nil, err
    }
    pkgList.PrepareIndex()
    return pkgList, nil
}

func hasPkg(list []mobianPkg, name string) bool {
    for _, v := range list {
        if strings.Compare(v.name, name) == 0 {
            return true
        }
    }
    return false
}

func searchRepo(q string, pkgList *deb.PackageList) (*deb.PackageList, error) {
    qry, err := query.Parse(q) //"Name (% gnome-*)")
    if err != nil {
        return nil, err
    }
    res := pkgList.Scan(qry)
    return res, nil
}

func getDebianVersion(list *deb.PackageList, name string) string {
    q := fmt.Sprintf("Name (= %s)", name)
    ret := "-"
    res, err := searchRepo(q, list)
    if err != nil {
        return "ERROR"
    }
    if res.Len() > 0 {
        _ = res.ForEach(func(debianpkg *deb.Package) error {
            ret = debianpkg.Version
            return nil
        })
    }
    return ret
}

func getUpstreamVersion(name string) string {
    version := "-"
    for _, value := range upstreamCsv {
        if value[0] == name {
            version = value[1]
            break
        }
    }
    return version
}

func printListToFile(fd *os.File, list []mobianPkg) {
    for _, pkg := range list {
        var pkgName string
        var cssClass string

        lastDebVersion := "-"
        if strings.Compare(pkg.dUnstable, "-") != 0 {
            lastDebVersion = pkg.dUnstable
        } else if strings.Compare(pkg.dTesting, "-") != 0 {
            lastDebVersion = pkg.dTesting
        }

        lagBehindDebian := false
        lagBehindUnstable := false
        lagBehindUpstream := false

        splitDebVersion := strings.Split(pkg.mUnstable, "-")

        if strings.Compare(lastDebVersion, "-") != 0 && deb.CompareVersions(pkg.mUnstable, lastDebVersion) < 0 {
            if strings.Contains(pkg.mUnstable, "~mobian") {
                splitMobianVersion := strings.Split(pkg.mUnstable, "~mobian")
                if deb.CompareVersions(splitMobianVersion[0], lastDebVersion) < 0 {
                    lagBehindDebian = true
                }
            } else {
                lagBehindDebian = true
            }
        }
        if deb.CompareVersions(pkg.mStable, pkg.mUnstable) < 0 {
            lagBehindUnstable = true
        }
        if deb.CompareVersions(splitDebVersion[0], pkg.upstream) < 0 {
            lagBehindUpstream = true
        }

        if lagBehindDebian {
            cssClass = " class=\"red\""
        } else if lagBehindUnstable {
            cssClass = " class=\"org\""
        } else {
            cssClass = ""
        }

        if strings.Compare(lastDebVersion, "-") != 0 {
            pkgName = fmt.Sprintf("<a href=\"https://tracker.debian.org/pkg/%s\"%s>%s</a>", pkg.name, cssClass, pkg.name)
        } else {
            pkgName = pkg.name
        }

        fmt.Fprintf(fd, "        <tr>\n")
        fmt.Fprintf(fd, "            <td%s>%s</td>\n", cssClass, pkgName)
        fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dTesting)
        fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.dUnstable)
        if lagBehindUnstable {
            fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.mStable)
        } else {
            fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.mStable)
        }
        if lagBehindDebian {
            fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.mUnstable)
        } else {
            fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.mUnstable)
        }
        if lagBehindUpstream {
            fmt.Fprintf(fd, "            <td class=\"prpl\">%s</td>\n", pkg.upstream)
        } else {
            fmt.Fprintf(fd, "            <td>%s</td>\n", pkg.upstream)
        }
        fmt.Fprintf(fd, "        </tr>\n")
    }
}

func createFile(path string, mainList, nonFreeList []mobianPkg) {
    fd, err := os.OpenFile(path, os.O_CREATE | os.O_TRUNC | os.O_WRONLY, 0644)
    if err != nil {
        fmt.Println("Unable to create file", path)
        os.Exit(1)
    }

    fmt.Fprintf(fd, `
<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            font-size: 11pt;
            border-collapse: collapse;
            table-layout: fixed;
        }
        td, th {
            border: 1px solid #000;
            text-align: left;
            padding: 8px;
        }
        th {
            text-align: center;
        }
        tr:nth-child(even) {
            background-color: #ddd;
        }
        a {
            color: #000;
        }
        .prpl, a.prpl {
            color: #b0b;
        }
        .red, a.red {
            color: #b00;
        }
        .org, a.org {
            color: #770;
        }
    </style>
</head>
<body>
    <h2>Mobian packages</h2>
    <span class="org">orange</span>&nbsp;-&nbsp;Version in Mobian stable is older than version in Mobian staging<br/>
    <span class="red">red</span>&nbsp;-&nbsp;Version in Mobian staging is older than most recent version in Debian<br/>
    <br/>
    <table>
        <tr>
            <th rowspan="2" colspan="1">Name</th>
            <th rowspan="1" colspan="2">Debian version</th>
            <th rowspan="1" colspan="2">Mobian version</th>
            <th rowspan="2" colspan="1">Upstream version</th>
        </tr>
        <tr>
            <th rowspan="1" colspan="1">Testing</th>
            <th rowspan="1" colspan="1">Unstable</th>
            <th rowspan="1" colspan="1">Stable</th>
            <th rowspan="1" colspan="1">Staging</th>
        </tr>
        <tr>
            <td colspan="6" style="font-size: 12pt; text-align: center"><b>main packages (%d)</b></td>
        </tr>
`, len(mainList))

    printListToFile(fd, mainList)

    fmt.Fprintf(fd, `
        <tr>
            <td colspan="6" style="font-size: 12pt; text-align: center"><b>non-free packages (%d)</b></td>
        </tr>
`, len(nonFreeList))

    printListToFile(fd, nonFreeList)

    fmt.Fprintf(fd, `
    </table>
</body>
</html>
`)
}

func populateMasterList(component string, db database.Storage) ([]mobianPkg, error) {
    var masterList []mobianPkg

    mobianPkgList, err := buildRepo("mobian", "https://repo.mobian.org", "bookworm", component, db)
    if err != nil {
        return nil, err
    }

    mobianPkgList.ForEach(func (pkg *deb.Package) error {
        if !hasPkg(masterList, pkg.Name) {
            newPkg := mobianPkg {
                name: pkg.Name,
                mStable: pkg.Version,
            }
            masterList = append(masterList, newPkg)
        }
        return nil
    })

    mobianUsPkgList, err := buildRepo("mobianUS", "https://repo.mobian.org", "staging", component, db)
    if err != nil {
        return nil, err
    }

    mobianUsPkgList.ForEach(func (pkg *deb.Package) error {
        if !hasPkg(masterList, pkg.Name) {
            newPkg := mobianPkg {
                name: pkg.Name,
                mStable: "-",
                mUnstable: pkg.Version,
            }
            masterList = append(masterList, newPkg)
        } else {
            for i, elem := range masterList {
                if strings.Compare(elem.name, pkg.Name) == 0 {
                    myPkg := &masterList[i]
                    myPkg.mUnstable = pkg.Version
                    break
                }
            }
        }
        return nil
    })

    debianTePkgList, err := buildRepo("debian", "https://deb.debian.org/debian", "testing", component, db)
    if err != nil {
        return nil, err
    }

    debianUsPkgList, err := buildRepo("debian", "https://deb.debian.org/debian", "unstable", component, db)
    if err != nil {
        return nil, err
    }

    for i, elem := range masterList {
        myPkg := &masterList[i]
        myPkg.dTesting = getDebianVersion(debianTePkgList, elem.name)
        myPkg.dUnstable = getDebianVersion(debianUsPkgList, elem.name)
        myPkg.upstream = getUpstreamVersion(elem.name)
    }

    sort.Slice(masterList, func(i, j int) bool {
        return strings.Compare(masterList[i].name, masterList[j].name) < 0
    })

    return masterList, nil
}

func main() {
    db, err := goleveldb.NewOpenDB("/tmp/listpkg/repo.db")
    if err != nil {
        log.Fatalln("%+v\n", err)
    }

    csvfile, err := os.Open("/var/www/packages/.upstream-versions")
    if err != nil {
        log.Fatalln("Couldn't open the csv file", err)
    }

    // Parse the file
    r := csv.NewReader(csvfile)
    upstreamCsv, err = r.ReadAll()
    if err != nil {
        log.Fatalln("Unable to read upstream versions", err)
    }

    mainList, err := populateMasterList("main", db)
    if err != nil {
        log.Fatalln("Unable to read main packages lists", err)
    }

    nonFreeList, err := populateMasterList("non-free", db)
    if err != nil {
        log.Fatalln("Unable to read non-free packages lists", err)
    }

    createFile(os.Args[1], mainList, nonFreeList)
}
