package main

import (
    "fmt"
    "io/ioutil"
    "log"
    "path"
    "os"
    "regexp"
    "strings"
    "net/http"
)

const (
    BUILD_NA int = iota
    BUILD_OK
    BUILD_FAILED
)

type Image struct {
    path string
    date string
    target string
    result int
}

type Package struct {
    path string
    date string
    time string
    pkg string
    version string
    result int
    amd64 int
    arm64 int
    armhf int
}

func buildStatus(dir string, arch string) int {
    content, err := ioutil.ReadFile(path.Join(dir, arch))
    if err != nil {
        return BUILD_NA
    }

    if strings.Contains(string(content), "Status: successful") {
        return BUILD_OK
    } else if strings.Contains(string(content), "not in arch list or does not match any arch wildcards") {
        return BUILD_NA
    }

    return BUILD_FAILED
}

func parseImage(dir string, date string, img string) (Image, error) {
    var image Image

    runes := []rune(date)
    image.date = fmt.Sprintf("%s-%s-%s", string(runes[0:4]), string(runes[4:6]), string(runes[6:8]))
    image.target = img

    image.path = path.Join(dir, date)

    content, err := ioutil.ReadFile(path.Join(dir, date, img + ".done"))
    if err != nil {
        image.result = BUILD_NA
        return image, err
    }

    if strings.Contains(string(content), "OK") {
        image.result = BUILD_OK
    } else {
        image.result = BUILD_FAILED
    }

    return image, nil
}

func listImages (basedir string) ([]Image, error) {
    var images []Image

    dates, _ := ioutil.ReadDir(basedir)

    for _, date := range dates {
        files, _ := ioutil.ReadDir(path.Join(basedir, date.Name()))
        for _, img := range files {
            if strings.HasSuffix(img.Name(), ".done") {
                image, _ := parseImage(basedir, date.Name(), strings.TrimSuffix(img.Name(), ".done"))
                images = append(images, image)
            }
        }
    }

    for i := len(images) / 2 - 1; i >= 0; i-- {
    	j := len(images) - 1 - i
    	images[i], images[j] = images[j], images[i]
    }

    return images, nil
}

func parsePackageVersion(filename string) string {
    file, err := ioutil.ReadFile(filename)
    if err != nil {
        return "-"
    }

    return string(file)
}

func parsePackage(dir string, date string, time string, pack string) (Package, error) {
    var pkg Package

    runes := []rune(date)
    pkg.date = fmt.Sprintf("%s-%s-%s", string(runes[0:4]), string(runes[4:6]), string(runes[6:8]))
    runes = []rune(time)
    pkg.time = fmt.Sprintf("%s:%s:%s", string(runes[0:2]), string(runes[2:4]), string(runes[4:6]))
    pkg.pkg = pack

    pkg.path = path.Join(dir, date, time, pkg.pkg)

    pkg.version = parsePackageVersion(path.Join(pkg.path, "version"))
    pkg.amd64 = buildStatus(pkg.path, "amd64.log")
    pkg.arm64 = buildStatus(pkg.path, "arm64.log")
    pkg.armhf = buildStatus(pkg.path, "armhf.log")

    content, err := ioutil.ReadFile(path.Join(pkg.path, "build.done"))
    if err != nil {
        if os.IsNotExist(err) {
            fmt.Println("Package", pkg.pkg, "build started on",
                        pkg.date, pkg.time, "not finished")
            err = nil
        }
        return pkg, err
    }

    if strings.Contains(string(content), "OK") {
        pkg.result = BUILD_OK
    } else {
        pkg.result = BUILD_FAILED
    }

    return pkg, nil
}

func listPackages (basedir string) ([]Package, error) {
    var packages []Package

    dates, _ := ioutil.ReadDir(basedir)

    for _, date := range dates {
        times, _ := ioutil.ReadDir(path.Join(basedir, date.Name()))
        for _, time := range times {
            pkgs, _ := ioutil.ReadDir(path.Join(basedir, date.Name(), time.Name()))
            for _, pkg := range pkgs {
                pack, _ := parsePackage(basedir, date.Name(), time.Name(), pkg.Name())
                packages = append(packages, pack)
            }
        }
    }

    for i := len(packages) / 2 - 1; i >= 0; i-- {
        j := len(packages) - 1 - i
        packages[i], packages[j] = packages[j], packages[i]
    }

    return packages, nil
}

func httpResponse(w http.ResponseWriter, r *http.Request) {
    dateReplacer := strings.NewReplacer("-", "")
    timeReplacer := strings.NewReplacer(":", "")

    fmt.Fprintf(w, `
<!DOCTYPE html>
<html>
<head>
  <style>
    table {
      font-family: arial, sans-serif;
      font-size: 11pt;
      border-collapse: collapse;
      width: 90%%;
    }
    td, th {
      border: 1px solid #000;
      text-align: left;
      padding: 8px;
    }
    tr:nth-child(even) {
      background-color: #ddd;
    }
    .ok {
      color: #090;
    }
    .fail, a.fail {
      color: #b00;
    }
    .na {
      color: #770;
    }
  </style>
</head>
<body>
<h2>Mobian build report</h2>
<span class="ok">OK</span>&nbsp;-&nbsp;Build succeeded<br/>
<span class="na">N/A</span>&nbsp;-&nbsp;Build is in progress or unneeded<br/>
<span class="fail">FAILED</span>&nbsp;-&nbsp;Build failed<br/>
`)

    dir := "/srv/builder/logs/images/"
    images, err := listImages(dir)
    if err != nil {
        fmt.Fprintf(w, "<h1>Error listing images in %s</h1>\n</body>\n</html>\n", dir)
        return
    }

    fmt.Fprintf(w, `
<div style="width:100%%;overflow:hidden;">
  <div style="width:30%%;float:left;">
  <h3>Images</h3>
  <table>
    <tr>
      <th>Date</th>
      <th>Target</th>
      <th>Status</th>
    </tr>
`)

    for _, img := range images {
        fmt.Fprintf(w, "    <tr>\n")
        fmt.Fprintf(w, "      <td>%s</td>\n", img.date)
        fmt.Fprintf(w, "      <td>%s</td>\n", img.target)
        if img.result == BUILD_OK {
            fmt.Fprintf(w, "      <td class=\"ok\">OK</td>\n")
        } else if img.result == BUILD_FAILED {
            fmt.Fprintf(w, "      <td><a class=\"fail\" href=\"/log?date=%s&img=%s\">FAILED</a></td>\n",
                        dateReplacer.Replace(img.date), img.target)
        } else {
            fmt.Fprintf(w, "      <td class=\"na\">N/A</td>\n")
        }

        fmt.Fprintf(w, "    </tr>\n")
    }
    fmt.Fprintf(w, "  </table>\n  </div>\n")

    dir = "/srv/builder/logs/packages/"
    packages, err := listPackages(dir)
    if err != nil {
        fmt.Fprintf(w, "</div>\n<h1>Error listing packages in %s</h1>\n</body>\n</html>\n", dir)
        return
    }

    fmt.Fprintf(w, `
  <div style="width:70%%;margin-left:30%%;">
  <h3>Packages</h3>
  <table>
    <tr>
      <th>Package</th>
      <th>Version</th>
      <th>Date</th>
      <th>Time</th>
      <th>Status</th>
      <th>amd64</th>
      <th>arm64</th>
      <th>armhf</th>
    </tr>
`)

    for _, pkg := range packages {
        fmt.Fprintf(w, "    <tr>\n")
        fmt.Fprintf(w, "      <td>%s</td>\n", pkg.pkg)
        fmt.Fprintf(w, "      <td>%s</td>\n", pkg.version)
        fmt.Fprintf(w, "      <td>%s</td>\n", pkg.date)
        fmt.Fprintf(w, "      <td>%s</td>\n", pkg.time)
        if pkg.result == BUILD_OK {
            fmt.Fprintf(w, "      <td class=\"ok\">OK</td>\n")
        } else if pkg.result == BUILD_FAILED {
            fmt.Fprintf(w, "      <td><a class=\"fail\" href=\"/log?date=%s&time=%s&pkg=%s&arch=build\">FAILED</a></td>\n",
                        dateReplacer.Replace(pkg.date), timeReplacer.Replace(pkg.time), pkg.pkg)
        } else {
            fmt.Fprintf(w, "      <td class=\"na\">N/A</td>\n")
        }
        if pkg.amd64 == BUILD_OK {
            fmt.Fprintf(w, "      <td class=\"ok\">OK</td>\n")
        } else if pkg.amd64 == BUILD_FAILED {
            fmt.Fprintf(w, "      <td><a class=\"fail\" href=\"/log?date=%s&time=%s&pkg=%s&arch=amd64\">FAILED</a></td>\n",
                        dateReplacer.Replace(pkg.date), timeReplacer.Replace(pkg.time), pkg.pkg)
        } else {
            fmt.Fprintf(w, "      <td class=\"na\">N/A</td>\n")
        }
        if pkg.arm64 == BUILD_OK {
            fmt.Fprintf(w, "      <td class=\"ok\">OK</td>\n")
        } else if pkg.arm64 == BUILD_FAILED {
            fmt.Fprintf(w, "      <td><a class=\"fail\" href=\"/log?date=%s&time=%s&pkg=%s&arch=arm64\">FAILED</a></td>\n",
                        dateReplacer.Replace(pkg.date), timeReplacer.Replace(pkg.time), pkg.pkg)
        } else {
            fmt.Fprintf(w, "      <td class=\"na\">N/A</td>\n")
        }
        if pkg.armhf == BUILD_OK {
            fmt.Fprintf(w, "      <td class=\"ok\">OK</td>\n")
        } else if pkg.armhf == BUILD_FAILED {
            fmt.Fprintf(w, "      <td><a class=\"fail\" href=\"/log?date=%s&time=%s&pkg=%s&arch=armhf\">FAILED</a></td>\n",
                        dateReplacer.Replace(pkg.date), timeReplacer.Replace(pkg.time), pkg.pkg)
        } else {
            fmt.Fprintf(w, "      <td class=\"na\">N/A</td>\n")
        }
        fmt.Fprintf(w, "    </tr>\n")
    }
    fmt.Fprintf(w, `
  </table>
  </div>
</div>
</body>
</html>
`)
}

func showLog(w http.ResponseWriter, r *http.Request) {
    var logdate string
    var logtime string
    var logitem string
    var logarch string
    var logfile string
    var img Image
    var pkg Package

    showPackage := true

    if r.Method != "GET" {
        fmt.Fprintf(w, "Sorry, only GET method is authorized!")
        return
    }

    err := r.ParseForm()
    if err != nil {
        fmt.Fprintf(w, "Unable to parse query parameters!")
        return
    }

    if len(r.Form["date"]) == 0 {
        fmt.Fprintf(w, "Missing 'date' parameter!")
        return
    }

    if len(r.Form["pkg"]) == 0 {
        if len(r.Form["img"]) == 0 {
            fmt.Fprintf(w, "Missing 'pkg' or 'img' parameter!")
            return
        }
        showPackage = false
    }

    logdate = r.Form["date"][0]

    if showPackage == true {
        if len(r.Form["time"]) == 0 {
            fmt.Fprintf(w, "Missing 'time' parameter!")
            return
        }

        if len(r.Form["arch"]) == 0 {
            fmt.Fprintf(w, "Missing 'arch' parameter!")
            return
        }

        logtime = r.Form["time"][0]
        logitem = r.Form["pkg"][0]
        logarch = r.Form["arch"][0]
    } else {
        logitem = r.Form["img"][0]
    }

    match, err := regexp.Match("[^0-9]", []byte(logdate))
    if match == true {
        fmt.Fprintf(w, "Unrecognized date format '%s'!", logdate)
        return
    }

    match, err = regexp.Match("[^a-z0-9+\\-.]", []byte(logitem))
    if match == true {
        fmt.Fprintf(w, "Unrecognized item name '%s'!", logitem)
        return
    }

    if showPackage == true {
        if logarch != "build" && logarch != "amd64" && logarch != "arm64" && logarch != "armhf" {
            fmt.Fprintf(w, "Unrecognized arch value '%s'!", logarch)
            return
        }

        match, err = regexp.Match("[^0-9]", []byte(logtime))
        if match == true {
            fmt.Fprintf(w, "Unrecognized time format '%s'!", logtime)
            return
        }

        pkg, err = parsePackage("/srv/builder/logs/packages/", logdate, logtime, logitem)
        if err != nil {
            fmt.Fprintf(w, "Unable to parse package '%s' built on %s at %s!", logitem, logdate, logtime)
            return
        }

        logfile = path.Join(pkg.path, logarch + ".log")
    } else {
        img, err = parseImage("/srv/builder/logs/images/", logdate, logitem)
        if err != nil {
            fmt.Fprintf(w, "Unable to parse image '%s' built on %s!", logitem, logdate)
            return
        }

        logfile = path.Join(img.path, logitem + ".log")
    }

    content, err := ioutil.ReadFile(logfile)
    if (err != nil) {
        fmt.Fprintf(w, "Unable to read logs for '%s' built on %s!", logitem, logdate)
        return
    }

    fmt.Fprintf(w, `
<!DOCTYPE html>
<html>
<body>
  <h2>Mobian build log</h2>
`)

    if showPackage == true {
        fmt.Fprintf(w, `
  <p>Package: %s v%s<br/>
  Build date: %s %s</p>
`, pkg.pkg, pkg.version, pkg.date, pkg.time)
    } else {
        fmt.Fprintf(w, `
  <p>Image: %s<br/>
  Build date: %s</p>
`, img.target, img.date)
    }

    fmt.Fprintf(w, `
  <textarea readonly="true" style="width:100%%;height:640px;">
%s
  </textarea>
</body>
</html>
`, string(content))
}

func main () {

    http.HandleFunc("/", httpResponse)
    http.HandleFunc("/log", showLog)
    log.Fatal(http.ListenAndServe(":3969", nil))

    return
}
